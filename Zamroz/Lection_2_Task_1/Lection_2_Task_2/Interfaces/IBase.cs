﻿using Lection_2_Task_2.Classes;

namespace Lection_2_Task_2.Interfaces
{
    public interface IBase
    {
        string Name { get; set; }
    }
}

﻿using Lection_2_Task_2.Classes;

namespace Lection_2_Task_2.Interfaces
{
    public interface IBook
    {
        string Name { get; set; }
        BaseAuthor Author { get; set; }
        BaseDepartment Department { get; set; }
        int Pages { get; set; }
    }
}

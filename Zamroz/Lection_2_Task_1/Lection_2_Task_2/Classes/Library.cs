﻿using Lection_2_Task_2.Interfaces;
using System.Linq;

namespace Lection_2_Task_2.Classes
{
    public class Library : BaseLibrary, ICountingBooks
    {
        public Library(string name)
        {
            Name = name;
        }
        public int BooksCount()
        {
            return LibraryList.Sum(data => data.BooksCount());
        }

        public override string ToString()
        {
            return Name + LibraryList.Aggregate("", (current, dep) => current + ("\n\t* " + dep.ToString()));
        }

    }
}

﻿using System.Collections.Generic;
using Lection_2_Task_2.Interfaces;

namespace Lection_2_Task_2.Classes
{
    public class BaseDepartment : ICountingBooks, IBase
    {
        public List<IBook> DepartmentList;

        protected BaseDepartment()
        {
             DepartmentList= new List<IBook>();
        }
        public string Name { get; set; }
        public void Add(IBook book)
        {
            DepartmentList.Add(book);
        }
        public int BooksCount()
        {
            return DepartmentList.Count;
        }
    }
}
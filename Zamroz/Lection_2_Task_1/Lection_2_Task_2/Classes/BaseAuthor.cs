﻿using System.Collections.Generic;
using Lection_2_Task_2.Interfaces;

namespace Lection_2_Task_2.Classes
{
    public abstract class BaseAuthor : IBase
    {
            public List<IBook> BooList;

            protected BaseAuthor()
            {
                BooList = new List<IBook>();
            }
            public string Name { get; set; }
            public void Add(IBook book)
            {
                BooList.Add(book);
            }

        
    }

}
﻿using Lection_2_Task_2.Interfaces;
using System;
using System.Linq;

namespace Lection_2_Task_2.Classes
{
    public class Department : BaseDepartment, IComparable<Department>
    {
        public Department(string name)
        {
            Name = name;
        }
        public int CompareTo(Department other)
        {
            return DepartmentList.Count.CompareTo(other.DepartmentList.Count);
        }

        public override string ToString()
        {
            return Name + DepartmentList.Aggregate("", (current, book) => current + ("\n\t\t* " + book.ToString()));
        }
    }

}


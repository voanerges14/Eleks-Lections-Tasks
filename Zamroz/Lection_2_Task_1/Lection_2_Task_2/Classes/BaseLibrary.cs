﻿using System.Collections.Generic;
using Lection_2_Task_2.Interfaces;

namespace Lection_2_Task_2.Classes
{
    public class BaseLibrary : IBase
    {
        public List<BaseDepartment> LibraryList;

        protected BaseLibrary()
        {
            LibraryList = new List<BaseDepartment>();
        }
        public string Name { get; set; }
        public void Add(BaseDepartment department)
        {
            LibraryList.Add(department);
        }
    }
}
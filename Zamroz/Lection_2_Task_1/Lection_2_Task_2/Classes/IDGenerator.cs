﻿using System;

namespace Lection_2_Task_2.Classes
{
    public abstract class IdGenerator
    {
        public string Id { get; set; }
        protected IdGenerator()
        {
            Id = new Guid().ToString();
        }
    }
}

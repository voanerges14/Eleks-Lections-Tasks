﻿using Lection_2_Task_2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lection_2_Task_2.Classes
{
    public class Author : BaseAuthor, IComparable<Author>, ICountingBooks
    {
        public Author(string name)
        {
            Name = name;
        }
        public int CompareTo(Author other)
        {
            return BooList.Count.CompareTo(other.BooList.Count);
        }
        public int BooksCount()
        {
            return BooList.Count;
        }

    }
}

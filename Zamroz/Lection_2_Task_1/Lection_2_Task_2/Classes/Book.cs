﻿using Lection_2_Task_2.Interfaces;
using System;

namespace Lection_2_Task_2.Classes
{
    public class Book : IdGenerator, IBook, IComparable<Book>
    {
        public Book(string name, int pages)
        {
            Pages = pages;
            Name = name;
        }
        public Book(string name, BaseAuthor author, BaseDepartment department, int pages)
        {
            Author = author;
            Department = department;
            Name = name;
            Pages = pages;
            author.Add(this);
        }
        public string Name { get; set; }
        public BaseAuthor Author { get; set; }
        public BaseDepartment Department { get; set; }
        public int Pages { get; set; }

        public int CompareTo(Book other)
        {
            return Pages.CompareTo(other.Pages);
        }
        public override string ToString()
        {
            return Name + "\t"
                + "\n\t\t\t* " + Author.Name + " " + Pages + " pages.";
        }
    }
}

﻿using Lection_2_Task_2;
using Lection_2_Task_2.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Lection_2_Task_2.XMLClasses.Classes;

namespace Lection_2_Task_2_Driver
{
    public class Driver
    {
        private static void Main(string[] args)
        {
            Department dep = new Department("Trillers");
            Department romances = new Department("Romances");

            Author at = new Author("Kidruk");
            Author Vinnitchuk = new Author("Vinnitchuk");

            Book book1 = new Book("Bot_1", at, dep, 12);
            Book book2 = new Book("Bot_2", at, dep, 22);
            Book book3 = new Book("Tverdinya", at, dep, 32);
            Book book4 = new Book("OnTheSky", at, dep, 42);

            Book romance1 = new Book("Romance_1", Vinnitchuk, romances, 124);
            Book romance2 = new Book("Romance_2", Vinnitchuk, romances, 223);
            Book romance3 = new Book("Romance_3", Vinnitchuk, romances, 332);
            Book romance4 = new Book("Romance_4", Vinnitchuk, romances, 442);

            dep.Add(book1);
            dep.Add(book2);
            dep.Add(book3);
            dep.Add(book4);
            Console.WriteLine(romance1.Id);
            Library lib_1 = new Library("Lib_1");
            lib_1.Add(dep);
            lib_1.Add(romances);

            Console.WriteLine(lib_1.ToString());
            Console.WriteLine(lib_1.BooksCount());

            BaseLoader loader = new BaseLoader();

            foreach (var book in loader.LoadBooks("Books.xml"))
            {
                Console.WriteLine(book.Name);
            }

            Console.ReadLine();
        }
    }
}

﻿
using System.Xml.Linq;

namespace Lection_2_Task_2.XMLClasses.Classes
{
    class CreateAllXMLs
    {
        public void CrateTemp()
        {
            XDocument libraryDocument = new XDocument(
    new XElement("libraries",
        new XElement("library",
            new XAttribute("name", "Lib_1")),
        new XElement("library",
            new XAttribute("name", "Lib_2")),
        new XElement("library",
            new XAttribute("name", "Lib_3"))
    )
);
            XDocument depDocument = new XDocument(
                new XElement("departments",
                    new XElement("department",
                        new XAttribute("name", "Thrillers")),
                    new XElement("department",
                        new XAttribute("name", "Romances")),
                    new XElement("department",
                        new XAttribute("name", "Comedies"))
                )
            );

            XDocument booksDocument = new XDocument(
                new XElement("books",
                    new XElement("book",
                        new XAttribute("name", "Bot_1"), new XAttribute("pages", "456")),
                    new XElement("book",
                        new XAttribute("name", "Bot_2"), new XAttribute("pages", "400")),
                    new XElement("book",
                        new XAttribute("name", "Bot_3"), new XAttribute("pages", "412"))
                )
            );

            XDocument authorDocument = new XDocument(
                new XElement("authors",
                    new XElement("author",
                        new XAttribute("name", "Kidruk")),
                    new XElement("book",
                        new XAttribute("name", "Vinnitshuk")),
                    new XElement("book",
                        new XAttribute("name", "Loza"))
                )
            );



            libraryDocument.Save("Libraries.xml");
            depDocument.Save("Department.xml");
            booksDocument.Save("Books.xml");
            authorDocument.Save("Author.xml");


        }
    }
}

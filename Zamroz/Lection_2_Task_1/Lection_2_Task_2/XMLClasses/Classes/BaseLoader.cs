﻿using System.Collections.Generic;
using System.Xml.Linq;
using Lection_2_Task_2.Classes;

namespace Lection_2_Task_2.XMLClasses.Classes
{
    class BaseLoader
    {
        public List<Book> LoadBooks(string filename)
        {
            XDocument doc = XDocument.Load(filename);
            List<Book> books = new List<Book>();
            foreach (XElement el in doc.Root.Elements())
            {
                books.Add(new Book(el.Attribute("name").Value, int.Parse(el.Attribute("pages").Value)));
            }
            return books;
        }

        public List<Author> LoadAuthors(string filename)
        {
            XDocument doc = XDocument.Load(filename);
            List<Author> authors = new List<Author>();
            foreach (XElement el in doc.Root.Elements())
            {
                authors.Add(new Author(el.Attribute("name").Value));
            }
            return authors;
        }
        public List<Library> LoadLibraryes(string filename)
        {
            XDocument doc = XDocument.Load(filename);
            List<Library> libraries= new List<Library>();
            foreach (XElement el in doc.Root.Elements())
            {
                libraries.Add(new Library(el.Attribute("name").Value));
            }
            return libraries;
        }
        public List<Department> LoadDepartments(string filename)
        {
            XDocument doc = XDocument.Load(filename);
            List<Department> departments = new List<Department>();
            foreach (XElement el in doc.Root.Elements())
            {
                departments.Add(new Department(el.Attribute("name").Value));
            }
            return departments;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Lection_2_Task_2.XMLClasses
{
    interface ILoaderXml
    {
        XDocument LoadBook(string url);
        XDocument LoadBase(string url);
        XDocument LoadAll(string url);
    }
}

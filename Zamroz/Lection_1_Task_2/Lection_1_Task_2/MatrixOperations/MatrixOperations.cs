﻿using System;
using System.Linq;


namespace Lection_1_Task_2.Matrix
{
    public class MatrixOperations
    {      
        public int[] SaddlePoint(int[][] Matrix)
        {
            int[] ColumeMax = new int[Matrix[0].Length];
            int[] Saddle = new int[0];
            int temp = 0;
            for (int i = 0; i < Matrix[0].Length; i++)
            {
                int Max = Matrix[0][i];
                for (int j = 1; j < Matrix.Length; j++)
                {
                    if (Matrix[j][i] > Max)
                    {
                        Max = Matrix[j][i];
                    }
                }
                ColumeMax[i] = Max;
            }
            foreach (int []i in Matrix)
            {
                foreach (int j in ColumeMax)
                {
                    if(i.Min() == j)
                    {
                        Array.Resize(ref Saddle, Saddle.Length + 1);
                        Saddle[temp++] = j;
                    }
                }
            }
            return Saddle;
        }

        public int[] SumRowWhereNegativeElement(int[][] Matrix)
        {
            int[] Sum = new int[0];
            int i = 0;
            foreach (int[] Mas in Matrix)
            {
                foreach (int Elem in Mas)
                {
                    if (Elem < 0)
                    {
                        Array.Resize(ref Sum, Sum.Length + 1);
                        Sum[i++] = Mas.Sum();
                        break;
                    }
                }
            }
            return Sum;
        }

        public int[][] SortMatrix(int[][] Aa)
        {
            int[][] A = (int[][])Aa.Clone();
            for (int i = 0; i < A.GetLength(0); i++)
            {
                for (int j = 0; j < A.GetLength(0) - i - 1; j++)
                {
                    if (CountDublicate(A[j]) < CountDublicate(A[j + 1]))
                    {
                        int[] temp = A[j];
                        A[j] = A[j + 1];
                        A[j + 1] = temp;
                    }
                }

            }
            return A;
        }

        private int CountDublicate(int[] Mas)
        {
            int Count = 0;
            int Max = 0;
            int [] MasWithoutDublicate = Mas.Distinct().ToArray();
            for (int i = 0; i < MasWithoutDublicate.Length; i++)
            {
                for (int j = 0; j < Mas.Length; j++)
                {
                    if (MasWithoutDublicate[i] == Mas[j])
                    {
                        Count++;
                    }
                }
                if(Count > Max)
                {
                    Max = Count;       
                }
                Count = 0;

            }
            return Max;
        }

        public int IsNoNegativeInColume(int[][] A)
        { 
            for (int i = 0; i < A[0].Length; i++)
            {
                for (int j = 0; j < A.Length; j++)
                {
                    if (A[j][i] < 0)
                    {
                        break;
                    }
                    if (j == A.Length - 1)
                    {
                        return i;
                    }
                        
                }

            }
            return -1;
        }

        public int[] CompareRowWithColum(int [][] A)
        {
            var kMas = new int[0];
            if (A.Length == A[0].Length)
            {
                for (var i = 0; i < A.Length; i++)
                {
                    for (var j = 0; j < A.Length; j++)
                    {
                        if (A[i][j] != A[j][i])
                        {
                            break;
                        }
                        if (j == A.Length - 1)
                        {
                            Array.Resize(ref kMas, kMas.Length + 1);
                            kMas[kMas.Length - 1] = i;
                        }

                    }

                }
            }
            return kMas;
        }

    }
}

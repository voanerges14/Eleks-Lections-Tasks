﻿using Lection_1_Task_2.Matrix;
using Lection_1_Task_2.MatrixWriter;
using System;

namespace Lection_1_Task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            MatrixOperations Matrix = new MatrixOperations();
            Writer writeM = new Writer();

            int[][] matrix1 = writeM.GenerateRandom(8, 8, -1, 5);
            Console.WriteLine("Origin Matrix");
            writeM.Write(matrix1);
            Console.WriteLine("Sorted by rows Matrix");
            writeM.Write(Matrix.SortMatrix(matrix1));
            Console.WriteLine("Index where no negative");
            Console.WriteLine(Matrix.IsNoNegativeInColume(matrix1));
            Console.WriteLine("Saddle Poinst");
            writeM.Write(Matrix.SaddlePoint(matrix1));
            Console.WriteLine("Sum row where negative element");
            writeM.Write(Matrix.SumRowWhereNegativeElement(matrix1));
            Console.WriteLine("Find k");
            writeM.Write(Matrix.CompareRowWithColum(matrix1));

            Console.ReadLine();

        }
    }
}

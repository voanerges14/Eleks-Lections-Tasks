﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lection_1_Task_2.MatrixWriter
{
    class Writer
    {
        public void Write(int [][] Matrix)
        {
            foreach(int [] mas in Matrix)
            {
                foreach (int i in mas)
                {
                    Console.Write(i + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
        public void Write(int [] Mas)
        {
            foreach(int i in Mas)
            {
                Console.Write(i + " ");
            }
            Console.WriteLine();
        }
        public int[][] GenerateRandom(int a, int b, int RandFrom, int RandTo)
        {
            Random Rand = new Random();
            int[][] A = new int[a][];
            for (int i = 0; i < A.Length; i++)
            {
                A[i] = new int[b];
                for (int j = 0; j < A[i].Length; j++)
                {
                    A[i][j] = Rand.Next(RandFrom, RandTo);
                }
            }
            return A;
        }

    }
}

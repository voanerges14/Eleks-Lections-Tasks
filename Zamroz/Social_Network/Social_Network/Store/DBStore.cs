﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Social_Network.BaseModel;
using Social_Network.Model;

namespace Social_Network.Store
{
    public class DBStore : IStorage
    {
        private const string TableName = "TEST";
        private const string FirstName = "FirstName";
        private const string LastName = "LastName";
        private const string Id = "Id";
        private string connectionString;
        private SqlConnection sqlConnection;
        private SqlCommand sqlCommand;
        private SqlDataAdapter sqlDataAdapter;
        private DataTable dataTable;

        public List<IBaseUser> users;

        public DBStore()
        {
            connectionString = @"Data Source=DESKTOP-QREQ7SH\SQLEXPRESS;Initial Catalog=SocialNetworkTest;Integrated Security=True";
            sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
        }

        public void Add(IBaseUser user)
        {
            string queryString = $"INSERT ";
        }

        public void Update(IBaseUser user)
        {
            throw new System.NotImplementedException();
        }

        public void Delete(string id)
        {
            throw new System.NotImplementedException();
        }

        public IBaseUser GetUser(string id)
        {
            string queryString = $"SELECT * FROM {TableName} WHERE id = {id}";
            using (sqlCommand= new SqlCommand(queryString, sqlConnection))
            {
                sqlCommand.CommandText = queryString;
                sqlCommand.ExecuteNonQuery();
                using (var reader = sqlCommand.ExecuteReader())
                {
                    return new User()
                    {
                        Id = reader.GetName(0),
                        FirstName = reader.GetName(1),
                        LastName = reader.GetName(2)
                    };
                }
            }

        }

        public List<IBaseUser> GetAll()
        {
            using (var reader = sqlCommand.ExecuteReader())
            {
                users = new List<IBaseUser>();
                while (reader.Read())
                {
                    IBaseUser person = new User()
                    {
                        Id = reader[DBStore.Id].ToString(),
                        FirstName = reader[DBStore.FirstName].ToString(),
                        LastName = reader[DBStore.LastName].ToString()
                    };
                    users.Add(person);
                }
}
            return users;
        }
    }
}

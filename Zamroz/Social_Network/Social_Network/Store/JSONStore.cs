﻿using System.Collections.Generic;
using System.Linq;
using Social_Network.BaseModel;

namespace Social_Network.Store
{
    public class JSONStore : BaseJSONStore, IStorage
    {
        private List<IBaseUser> users;
        
        public JSONStore() : base()
        {
            users = new List<IBaseUser>();
        }
        public void Add( IBaseUser user)
        {
            users.Add(user);
            WriteFile(users);
        }

        public void Update( IBaseUser editeUseruser)
        {
            for (var i = 0; i < users.Count; i++)
            {
                if (users[i].Id == editeUseruser.Id)
                {
                    users[i] = editeUseruser;
                }
            }
        }

        public void Delete( string id)
        {
            for (var i = 0; i < users.Count; i++)
            {
                if (users[i].Id == id)
                {
                    users.Remove(users[i]);
                }
            }
        }

        public IBaseUser GetUser( string id)
        {
            ReadFile();
            return users.FirstOrDefault(user => user.Id == id);
        }

        public List<IBaseUser> GetAll()
        {
            return users;
        }
    }
}

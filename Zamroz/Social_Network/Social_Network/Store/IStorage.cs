﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Social_Network.BaseModel;

namespace Social_Network.Store
{
    public interface IStorage
    {
        void Add(IBaseUser user);
        void Update(IBaseUser user);
        void Delete(string id);
        IBaseUser GetUser(string id);
        List<IBaseUser> GetAll();

    }
}

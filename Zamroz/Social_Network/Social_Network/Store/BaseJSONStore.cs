﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Social_Network.BaseModel;
using Social_Network.Model;

namespace Social_Network.Store
{
    public abstract class BaseJSONStore
    {
        private const string FileName = "users.txt";
        private JavaScriptSerializer jsonSerialiser;

        protected BaseJSONStore()
        {
            jsonSerialiser = new JavaScriptSerializer();
        }
        protected void WriteFile(List<IBaseUser> users)
        {
            using (var writer = new StreamWriter(FileName))
            {
                var json = jsonSerialiser.Serialize(users);
                writer.WriteLine(json);
            }
        }

        protected List<User> ReadFile()
        {
            using (var reader = new StreamReader(FileName))
            {
                var jsonResponse = reader.ReadToEnd();
                return jsonSerialiser.Deserialize<List<User>>(jsonResponse);
            }
        }
    }
}

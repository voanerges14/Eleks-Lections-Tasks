﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Social_Network.BaseModel;

namespace Social_Network.Model
{
    public class User : IBaseUser
    {
        public User()
        {
            Id = Guid.NewGuid().ToString();
        }

        public User(string FName, string LName) : this()
        {
            FirstName = FName;
            LastName = LName;
        }
        public User(string id,string FName, string LName)
        {
            Id = id;
            FirstName = FName;
            LastName = LName;
        }


        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}

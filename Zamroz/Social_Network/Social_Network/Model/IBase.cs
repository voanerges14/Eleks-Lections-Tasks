﻿namespace Social_Network.BaseModel
{
    public interface IBaseUser
    {
        string Id { get; set; }
        string FirstName  { get; set; }
        string LastName  { get; set; }
    }
}

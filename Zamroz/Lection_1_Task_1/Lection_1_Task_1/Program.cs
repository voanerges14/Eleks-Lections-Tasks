﻿using Lection_1_Task_1;
using Lection_1_Task_1.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Lection_1_Task_1.Classes;

namespace Lection_1_Task_1
{
    public class Driver
    {
        public static Author BestAuthor(Library library)
        {
            var max = 0;
            Author obj = null;
            foreach (var dep in library.Data)
            {
                foreach (var book in dep.Data)
                {
                    if (max >= book.Author.Data.Length) continue;
                    max = book.Author.Data.Length;
                    obj = book.Author;
                }
            }
            return obj;
        }
        public static Department BestDepartment(Library library)
        {
            var max = 0;
            Department obj = null;
            foreach (var dep in library.Data)
            {
                if (max >= dep.Data.Length) continue;
                max = dep.Data.Length;
                obj = dep;
            }
            return obj;
        }
        public static Book SmallerBook(Library library)
        {
            var min = int.MaxValue;
            Book obj = null;
            foreach (var dep in library.Data)
            {
                foreach (var book in dep.Data)
                {
                    if (min <= book.Pages) continue;
                    min = book.Pages;
                    obj = book;
                }
            }
            return obj;
        }
        static void Main(string[] args)
        {
            Library library_1 = new Library("Lib_1");
            Library library_2 = new Library("Lib_2");

            Department Thrillers = new Department("Thrillers");
            Department Romans = new Department("Romans");
            Department Comedy = new Department("Comedy");

            Author Kidruk = new Author("M.Kidruk");
            Author Vinnishuk = new Author("V.Vinnishuk");

            Book bot_1 = new Book("Bot_1", Kidruk, Thrillers, 400);
            Book bot_2 = new Book("Bot_2", Kidruk, Thrillers, 450);
            Book bot_3 = new Book("Bot_3", Kidruk, Thrillers, 450);
            Book Malva_Landa = new Book("Malva_Landa", Vinnishuk, Romans, 1024);

            Thrillers.Add(bot_1);
            Thrillers.Add(bot_2);
            Thrillers.Add(bot_3);

            Romans.Add(Malva_Landa);

            library_1.Add(Thrillers);
            library_1.Add(Romans);
            library_2.Add(Comedy);

            Console.WriteLine(library_1);
            Console.WriteLine(library_2);
            Console.WriteLine("Thrillers have: {0} books", Thrillers.BooksCount());
            Console.WriteLine("{0} has: {1} books", library_1.Name, library_1.BooksCount());
            Console.WriteLine("Best Author: {0}",BestAuthor(library_1).Name);
            Console.WriteLine("Best Department: {0}", BestDepartment(library_1).Name);
            Console.WriteLine("Smaller book: {0}", SmallerBook(library_1).Name);

            Console.WriteLine(Romans.CompareTo(Thrillers));
            foreach (var book in Kidruk.Data)
            {
                Console.WriteLine("Authors books: " + book.Name);
            }
            foreach (var book in Thrillers.Data)
            {
                Console.WriteLine("Departmens books: " + book.Name);
            }


            Console.ReadLine();
        }
    }
}

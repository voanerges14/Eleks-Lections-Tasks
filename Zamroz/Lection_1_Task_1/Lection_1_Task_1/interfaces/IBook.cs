﻿using Lection_1_Task_1.Classes;

namespace Lection_1_Task_1.interfaces
{
    public interface IBook
    {
        string Name { get; set; }
        Author Author { get; set; }
        Department Department { get; set; }
        int Pages { get; set; }
    }
}

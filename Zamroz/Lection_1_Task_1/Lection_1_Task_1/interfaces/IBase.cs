﻿namespace Lection_1_Task_1.interfaces
{
    public interface IBase
    {
        string Name { get; set; }
        void Add(object obj);
    }
}

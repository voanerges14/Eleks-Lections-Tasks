﻿using System;
using Lection_1_Task_1.interfaces;

namespace Lection_1_Task_1.Classes
{
    public class Book : IBook, IComparable<Book>
    {
        private readonly int _i = 0;
        public Book(string name, Author author, Department dep, int pages)
        {
            Author = author;
            Department = dep;
            Name = name;
            Pages = pages;
            author.Add(this);
        }
        public string Name { get; set; }
        public Author Author { get; set; }
        public Department Department { get; set; }
        public int Pages { get; set; }

        public int CompareTo(Book other)
        {
            return Pages.CompareTo(other.Pages);
        }
        public override string ToString()
        {
            return Name + "\t"
                + "\n\t\t\t* " + Author.Name + " " + Pages + " pages.";
        }
    }

}

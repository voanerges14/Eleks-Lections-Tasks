﻿using System;
using Lection_1_Task_1.interfaces;

namespace Lection_1_Task_1.Classes
{
    public abstract class Container<T> : IBase
    {
        public T[] Data;

        protected Container()
        {
            Data = new T[0];
        }
        public string Name { get; set; }
        public void Add(object obj)
        {
            Array.Resize(ref Data, Data.Length + 1);
            Data[Data.Length - 1] = (T)obj;
        }
    }
}

﻿using System;
using Lection_1_Task_1.interfaces;

namespace Lection_1_Task_1.Classes
{
    public class Author : Container<Book>, IComparable<Author>, ICountingBooks
    {
        public Author(string name)
        {
            Name = name;
        }
        public int CompareTo(Author other)
        {
            return Data.Length.CompareTo(other.Data.Length);
        }

        public int BooksCount()
        {
            return Data.Length;
        }

        
    }
}

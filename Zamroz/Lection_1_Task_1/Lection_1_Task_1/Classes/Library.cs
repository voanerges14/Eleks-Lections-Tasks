﻿using System.Linq;
using Lection_1_Task_1.interfaces;

namespace Lection_1_Task_1.Classes
{
    public class Library : Container<Department>, ICountingBooks
    {
        public Library(string name)
        {
            Name = name;
        }
        public int BooksCount()
        {
            return Data.Sum(data => data.BooksCount());
        }

        public override string ToString()
        {
            return Name + Data.Aggregate("", (current, dep) => current + ("\n\t* " + dep.ToString()));
        }

    }
}

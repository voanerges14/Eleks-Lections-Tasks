﻿using System;
using System.Linq;
using Lection_1_Task_1.interfaces;


namespace Lection_1_Task_1.Classes
{
    public class Department : Container<Book>, IComparable<Department>, ICountingBooks
    {
        public Department(string name)
        {
            Name = name;
        }
        public int CompareTo(Department other)
        {
            return Data.Length.CompareTo(other.Data.Length);
        }

        public int BooksCount()
        {
             return Data.Length;
        }
        public override string ToString()
        {
            var str = Data.Aggregate("", (current, book) => current + ("\n\t\t* " + book.ToString()));
            return Name + "\t" + str;
        }
    }
}
